package com.example.martinj.redditearthporn;

/**
 * Created by martinj on 2015-07-08.
 */
public class ListItem {
    public String thumbnailUrl;
    public String description;
    public String permalink;

    public ListItem (String url, String description, String permalink) {
        this.thumbnailUrl = url;
        this.description = description;
        this.permalink = permalink;
    }
}
