package com.example.martinj.redditearthporn;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import redditpojo.RedditChildrenPojo;
import redditpojo.RedditPojo;

/**
 * Created by martinj on 2015-07-08.
 */
public class ListFragment extends Fragment {

    private ArrayList<ListItem> myItemArray = new ArrayList<>();
    private ListAdapter customAdater;

    private String url = "https://www.reddit.com/r/EarthPorn/.json";

    // Create a message handling object as an anonymous class.
    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            // Do something in response to the click
            Intent intent = new Intent(getActivity(), WebViewActivity.class);
            intent.putExtra("permalink", customAdater.getItem(position).permalink);
            startActivity(intent);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        customAdater = new ListAdapter(getActivity().getApplicationContext()); // pass app context to image loader

        View rootView = inflater.inflate(R.layout.list_fragment, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(customAdater);
        customAdater.setItems(myItemArray);
        listView.setOnItemClickListener(mMessageClickedHandler);

        getJSONFromReddit();

        setHasOptionsMenu(false);

        return rootView;
    }

    private void getJSONFromReddit () {

//        JsonObjectRequest jsObjRequest = new JsonObjectRequest
//                (Request.Method.GET, url, new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d("Fr. REDDIT", response.toString());
//                        JSONObject data = response.optJSONObject("data");
//                        JSONArray jsonArray = data.optJSONArray("children");
//
//                        HashMap<String, String> applicationSettings = new HashMap<String,String>();
//                        for(int i = 0; i < jsonArray.length(); i++){
//                            try {
//                                JSONObject data2 = jsonArray.getJSONObject(i).getJSONObject("data");
//                                JSONObject preview = data2.getJSONObject("preview");
//                                JSONArray images = preview.optJSONArray("images");
//
//                                JSONObject source = images.getJSONObject(0).getJSONObject("source");
//                                String url = source.getString("url");
//                                String title = data2.getString("title");
//
//                                myItemArray.add(new ListItem(url, title));
//
//                            } catch (JSONException e) {
//
//                            }
//
//                        }
//
//                        customAdater.setItems(myItemArray);
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // TODO Auto-generated method stub
//                        Log.e("Fr. REDDIT", error.toString());
//                    }
//                });

        GsonRequest<RedditPojo> jsObjRequest = new GsonRequest<RedditPojo>
                (url, RedditPojo.class, null, new Response.Listener<RedditPojo>() {

                    @Override
                    public void onResponse(RedditPojo response) {
                        Log.d("Fr. REDDIT", response.toString());

                        for (RedditChildrenPojo childrenPojo : response.data.children) {
                            String url = childrenPojo.data.preview.images.get(0).source.url;
                            String title = childrenPojo.data.title;
                            String permalink = childrenPojo.data.permalink;

                            myItemArray.add(new ListItem(url, title, permalink));
                        }

                        customAdater.setItems(myItemArray);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.e("Fr. REDDIT", error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsObjRequest);
    }
}
