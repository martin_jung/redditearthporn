package com.example.martinj.redditearthporn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

/**
 * Created by martinj on 2015-07-09.
 */
public class WebViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        WebView webview = new WebView(this);
//        setContentView(webview);

        setContentView(R.layout.detail_view);
        WebView webview = (WebView) findViewById(R.id.webview);

        Intent intent = getIntent();
        String permalink = "http://m.reddit.com" + intent.getExtras().getString("permalink");

        Log.d("WEB VIEW", permalink);

//        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        webview.getSettings().setJavaScriptEnabled(true);

//        final Activity activity = this;
//        webview.setWebChromeClient(new WebChromeClient() {
//            public void onProgressChanged(WebView view, int progress) {
//                // Activities and WebViews measure progress with different scales.
//                // The progress meter will automatically disappear when we reach 100%
//                activity.setProgress(progress * 1000);
//            }
//        });
//        webview.setWebViewClient(new WebViewClient() {
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
//            }
//        });


        webview.loadUrl(permalink);
    }
}
