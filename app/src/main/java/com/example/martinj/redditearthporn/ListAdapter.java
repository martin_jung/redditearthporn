package com.example.martinj.redditearthporn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martinj on 2015-07-08.
 */
public class ListAdapter extends BaseAdapter {
    private List<ListItem> mItems = new ArrayList<>();
    private final Context mContext;
    private LayoutInflater inflater;

    private ImageLoader mImageLoader;

    public ListAdapter(Context context) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageLoader = MySingleton.getInstance(mContext).getImageLoader();
    }

    public void add(ListItem item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<ListItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public ListItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemViewHolder viewHolder;

        // convertView is the swapView (recycle)
        LinearLayout itemLayout = (LinearLayout) convertView;

        if (itemLayout == null) {
            itemLayout = (LinearLayout) inflater.inflate(R.layout.list_item, parent, false);

            viewHolder = new ItemViewHolder();
            viewHolder.thumbnail = (NetworkImageView) itemLayout.findViewById(R.id.thumbnail);
            viewHolder.description = (TextView) itemLayout.findViewById(R.id.description);
            itemLayout.setTag(viewHolder); // set tag for each row
        } else {
            viewHolder = (ItemViewHolder) itemLayout.getTag();
        }

        viewHolder.thumbnail.setImageUrl(getItem(position).thumbnailUrl, mImageLoader);
        viewHolder.description.setText(getItem(position).description);

        return itemLayout;
    }

    static class ItemViewHolder {
        NetworkImageView thumbnail;
        TextView description;
    }
}
