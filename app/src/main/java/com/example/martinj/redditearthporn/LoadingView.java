package com.example.martinj.redditearthporn;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by martinj on 2015-07-10.
 */
public class LoadingView extends RelativeLayout {

    private Animation zoomInAnimation;
    private Animation zoomOutAnimation;

    private Animator.AnimatorListener animationListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            Intent intent = new Intent(getContext(), MainActivity.class);
            getContext().startActivity(intent);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    public LoadingView(Context context) {
        super(context);
        initView(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.loading_detail, this);

        zoomInAnimation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
        this.zoomIn();


        ImageView cute1 = (ImageView) findViewById(R.id.cute1);
        createCircleAnimator(cute1, 0);

        ImageView cute2 = (ImageView) findViewById(R.id.cute2);
        createCircleAnimator(cute2, 200);

        ImageView cute3 = (ImageView) findViewById(R.id.cute3);
        createCircleAnimator(cute3, 400);

        ImageView cute4 = (ImageView) findViewById(R.id.cute4);
        createCircleAnimator(cute4, 600);

    }

    private void zoomIn() {
        startAnimation(zoomInAnimation);
    }

    private void createCircleAnimator(View view, long delay) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, "alpha", 0f);
        fadeOut.setDuration(2000);

        ObjectAnimator mover = ObjectAnimator.ofFloat(view, "translationX", 0f, 200f, 0f);
        mover.setDuration(2000);

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(view, "alpha", 1f);
        fadeIn.setDuration(2000);

        Keyframe kf0 = Keyframe.ofFloat(0f, 0f);
        Keyframe kf1 = Keyframe.ofFloat(.5f, 360f);
        Keyframe kf2 = Keyframe.ofFloat(1f, 0f);
        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe("rotation", kf0, kf1, kf2);
        ObjectAnimator rotationAnim = ObjectAnimator.ofPropertyValuesHolder(view, pvhRotation);
        rotationAnim.setDuration(2000);

        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.play(fadeOut).with(mover).before(fadeIn);
        animatorSet.play(rotationAnim);
        animatorSet.setStartDelay(delay);

        if (view.getId() == R.id.cute4) {
            animatorSet.addListener(animationListener);
        }

        animatorSet.start();

    }
}
