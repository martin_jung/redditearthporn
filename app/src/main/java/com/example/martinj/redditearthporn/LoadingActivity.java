package com.example.martinj.redditearthporn;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by martinj on 2015-07-10.
 */
public class LoadingActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
    }
}
